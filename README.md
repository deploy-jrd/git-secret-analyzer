How to install this for your repository
=======================================

With a local script
-------------------

`run_secrets_test`:
```sh
#!/bin/sh
set -e
HERE="$(readlink -f "$(dirname "$0")")"
exec docker run --rm -it -v "$HERE:/app:ro" registry.gitlab.com/deploy-jrd/git-secret-analyzer:1.0.0
```

Adjust the version to the latest available.

With Gitlab CI
--------------

`.gitlab-ci.yml`:
```yaml
secret:
  stage: test
  image:
    name: registry.gitlab.com/deploy-jrd/git-secret-analyzer:1.0.0
    entrypoint: ["/bin/bash", "-c", "exec bash /usr/local/bin/entrypoint"]
  variables:
    APP_DIR: "$CI_PROJECT_DIR"
  script:
    - '#'
```

Adjust the version to the latest available.

By default, all branches and tags are scanned. If you want to change this, specify the `LOG_OPTS` variable with the options that `git log` can take.

How to clean a repository with secrets
======================================

- First, edit and rebase the dirty commits.
- Push force the branch (or branches) involved.

This is not enough as the remote git will keep reference to the old commits.

To definitively delete those old commits, here what you can do with *gitlab*:
- Get the full `sha1` of your old commits (40 digits long)
- Create a text file named `commit-map`
- Put one old commit on one line using the following format:
  ```
  40-digits-commit-to-delete 0000000000000000000000000000000000000000
  ```
- Wait **30 minutes** after the last `git push`
- Go to `settings/repository` on *Gitlab* projet
- Go to `Repository cleanup` and uploade that `commit-map` file
